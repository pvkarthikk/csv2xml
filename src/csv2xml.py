#-------------------------------------------------------------------------------
# Name:        csv2xml.py
# Purpose:
#
# Author:      Karthik Venkatesh
#
# Created:     25-10-2019
# Copyright:   (c) pvkarthikk 2019
# Licence:     <your licence>
#-------------------------------------------------------------------------------
import os,sys,csv
import xml.etree.ElementTree as ET
from xml.etree import ElementTree
from xml.dom import minidom
class csv2xml():
    def __init__(self):
        pass
    def parseCSV(self,csvFile):
        ''' 
        Returns column names (list) and row contents(list) from the passed csvfile
        '''
        columns = [] 
        rows = []
        try:
            csvfile = open(csvFile, 'r')
        except IOError as error:
            print(error)
            sys.exit(0)
        csvreader = csv.reader(csvfile)
        columns = csvreader.next()
        for row in csvreader:
            rows.append(row)
        return columns,rows
    def generatexml(self,csvFile,xmlfile=None):
        # Parsing the csv file
        csvcolumns,csvrows = self.parseCSV(csvFile)
        # Create root element
        xmlcsv = ET.Element('csv')
        # Set root elemetn attrib 
        xmlcsv.attrib["name"] = os.path.basename(csvFile)
        # Create subelement columns for root
        xmlcolumns = ET.SubElement(xmlcsv,'columns')
        # Iterate through all columns names
        for col in csvcolumns:
            # Create subelement column for columns
            xmlcolumn = ET.SubElement(xmlcolumns,'column')
            xmlcolumn.text = col
        # Create subelement rows for root
        xmlrows = ET.SubElement(xmlcsv,'rows')
        for row in csvrows:
            i = len(csvcolumns) - 1
            # create subelement row for rows
            xmlrow = ET.SubElement(xmlrows,'row')
            while i >= 0:
                # create subelement as column name for row
                elem = ET.SubElement(xmlrow,str(csvcolumns[i]))
                elem.text = str(row[i])
                i -= 1
        # check output xml file available
        if xmlfile != None:
            output = open(xmlfile,'w+')
            output.write(self.prettify(xmlcsv))
            output.close()
        else:
            ET.dump(xmlcsv)
            pass
        return xmlcsv
    def prettify(self,elem):
        """Return a pretty-printed XML string for the Element.
        """
        rough_string = ElementTree.tostring(elem, 'utf-8')
        reparsed = minidom.parseString(rough_string)
        return reparsed.toprettyxml(indent="  ")
            
    pass
class xml2csv():
    def __init__(self):
        pass
    def generatecsv(self,xmlfile,csvfile=None):
        content = ''
        tree = ET.parse(xmlfile)
        root = tree.getroot()
        columns = root.find('columns')
        temp_col = []
        for col in columns:
            temp_col.append(col.text)
            content += col.text+','
        content = content[:-1]+'\n'
        rows = root.find('rows')
        for row in rows:
            for cols in temp_col:
                elem = row.find(cols)
                content += elem.text + ','
            content = content[:-1]+'\n'
        if csvfile != None:
            output = open(csvfile,'w+')
            output.write((content))
            output.close()
        else:
            print content
            pass
        return content
    pass
