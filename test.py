from src.csv2xml import csv2xml,xml2csv

if __name__ == "__main__":
    app = csv2xml() 
    case = app.generatexml('input_csv.csv','output_xml.xml')
    app = xml2csv()
    app.generatecsv('input_xml.xml','output_csv.csv')
    pass